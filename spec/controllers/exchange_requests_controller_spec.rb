RSpec.describe ExchangeRequestsController, type: :controller do
  describe 'GET index' do
    subject { -> { get :index } }

    it {
      subject.call
      expect(response.status).to eq(401)
    }

    context 'when request is authorized' do
      include_context 'with_authorized_user'

      it {
        subject.call
        expect(response.status).to eq(200)
      }

      it {
        subject.call
        expect(response.body).to eq([].to_json)
      }

      context "with exchange_request" do
        let!(:account_from) { create :account, currency: 'USD', user: User.first }
        let!(:account_to) { create :account, currency: 'CAD', user: User.first }
        let!(:exchange_request) { create :exchange_request, account_from: account_from, account_to: account_to }
        let(:expected_result) {
          [{account_from_id: account_from.id,
            account_to_id: account_to.id,
            account_to: {currency: "CAD"},
            account_from: {currency: "USD"},
            amount: 1000,
            created_at: be,
            datetime_before: be,
            id: be,
            max_exchange_rate: "1000.0",
            min_exchange_rate: "100.0",
            updated_at: be}.deep_stringify_keys]
        }
        it {
          subject.call
          expect(response.status).to eq(200)
        }

        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
      end
    end
  end

  describe 'POST create' do
    include_context 'with_authorized_user'

    subject { -> { post :create, params: params, as: :json } }

    let!(:exchange_rate) { create :exchange_rate }

    context 'with account_from' do
      let!(:account_from) { create :account, currency: 'USD', user: User.first }

      context 'when account_to created' do
        let(:params) { {
          currency_from: account_from.currency,
          currency_to: 'CAD',
          amount: 100,
          datetime_before: Time.zone.now + 1.week,
          min_exchange_rate: 10,
          max_exchange_rate: 100
        } }

        let(:expected_result) {
          {account_from_id: account_from.id,
           account_to_id: be,
           amount: 100,
           created_at: be,
           datetime_before: be,
           id: be,
           max_exchange_rate: "100.0",
           min_exchange_rate: "10.0",
           updated_at: be}.stringify_keys
        }

        it {
          subject.call
          expect(response.status).to eq(201)
        }
        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
        it { expect { subject.call }.to change(Account, :count).by(1) }
        it {
          expect(PerformExchangeRequestJob).to receive(:perform_later).once
          subject.call
        }
      end

      context 'with account_to' do
        let!(:account_to) { create :account, currency: 'CAD', user: User.first }

        let(:params) { {
          currency_from: account_from.currency,
          currency_to: account_to.currency,
          amount: 100,
          datetime_before: Time.zone.now + 1.week,
          min_exchange_rate: 10,
          max_exchange_rate: 100
        } }

        let(:expected_result) {
          {account_from_id: account_from.id,
           account_to_id: be,
           amount: 100,
           created_at: be,
           datetime_before: be,
           id: be,
           max_exchange_rate: "100.0",
           min_exchange_rate: "10.0",
           updated_at: be}.stringify_keys
        }

        it {
          subject.call
          expect(response.status).to eq(201)
        }
        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
        it { expect { subject.call }.not_to change(Account, :count) }
        it {
          expect(PerformExchangeRequestJob).to receive(:perform_later).once
          subject.call
        }
      end

      context 'without exchange rates bounds' do
        let(:params) { {
          currency_from: account_from.currency,
          currency_to: 'CAD',
          amount: 100,
          datetime_before: Time.zone.now + 1.week
        } }

        let(:expected_result) {
          {message: "You must enter at least one exchange rule"}.stringify_keys
        }
        it {
          subject.call
          expect(response.status).to eq(400)
        }
        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
        it { expect { subject.call }.not_to change(Account, :count) }
        it {
          expect(PerformExchangeRequestJob).not_to receive(:perform_later)
          subject.call
        }
      end

      context 'without datetime_before param' do
        let(:params) { {
          currency_from: 'USD',
          currency_to: 'CAD',
          amount: 100,
          min_exchange_rate: 10,
          max_exchange_rate: 100
        } }

        let(:expected_result) {
          {message: "You must enter datetime_before"}.stringify_keys
        }

        it {
          subject.call
          expect(response.status).to eq(400)
        }
        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
        it { expect { subject.call }.not_to change(Account, :count) }
        it {
          expect(PerformExchangeRequestJob).not_to receive(:perform_later)
          subject.call
        }
      end

      context 'when min_rate higher than max_rate' do
        let(:params) { {
          currency_from: account_from.currency,
          currency_to: 'CAD',
          amount: 100,
          datetime_before: Time.zone.now + 1.week,
          min_exchange_rate: 1000,
          max_exchange_rate: 100
        } }
        let(:expected_result) {
          {message: "Min rate can not be higher than max rate"}.stringify_keys
        }

        it {
          subject.call
          expect(response.status).to eq(400)
        }
        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
        it { expect { subject.call }.not_to change(Account, :count) }
        it {
          expect(PerformExchangeRequestJob).not_to receive(:perform_later)
          subject.call
        }
      end

      context 'when unknown currency came' do
        let(:params) { {
          currency_from: account_from.currency,
          currency_to: 'test_unknown',
          amount: 100,
          datetime_before: Time.zone.now + 1.week,
          min_exchange_rate: 1000,
          max_exchange_rate: 100
        } }
        let(:expected_result) {
          {message: "Corrupted currency"}.stringify_keys
        }

        it {
          subject.call
          expect(response.status).to eq(400)
        }
        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
        it { expect { subject.call }.not_to change(Account, :count) }
        it {
          expect(PerformExchangeRequestJob).not_to receive(:perform_later)
          subject.call
        }
      end
    end

    context 'without account_from' do
      let(:params) { {
        currency_from: 'USD',
        currency_to: 'CAD',
        amount: 100,
        datetime_before: Time.zone.now + 1.week
      } }

      let(:expected_result) {
        {message: "You do not have USD account"}.stringify_keys
      }
      it {
        subject.call
        expect(response.status).to eq(400)
      }
      it {
        subject.call
        expect(JSON.parse(response.body)).to match(expected_result)
      }
      it { expect { subject.call }.not_to change(Account, :count) }
      it {
        expect(PerformExchangeRequestJob).not_to receive(:perform_later)
        subject.call
      }
    end
  end

  describe 'DELETE destroy' do
    include_context 'with_authorized_user'

    subject { -> { delete :destroy, params: {id: exchange_request_id} } }

    context 'with existing exchange_request' do
      let!(:account_from) { create :account, currency: 'USD', user: User.first }
      let!(:account_to) { create :account, currency: 'CAD', user: User.first }
      let!(:exchange_request) { create :exchange_request, account_from: account_from, account_to: account_to }
      let!(:exchange_request_id) { exchange_request.id }

      it {
        subject.call
        expect(response.status).to eq(204)
      }
      it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
      it { expect { subject.call }.not_to change(Account, :count) }
    end

    context 'without existing exchange_request' do
      let!(:exchange_request_id) { -1 }

      it {
        subject.call
        expect(response.status).to eq(404)
      }
      it { expect { subject.call }.not_to change(ExchangeRequest, :count) }
    end
  end
end