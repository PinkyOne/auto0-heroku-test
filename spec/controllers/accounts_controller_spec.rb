RSpec.describe AccountsController, type: :controller do
  describe 'GET index' do
    subject { -> { get :index } }
    let(:account) { create :account, user: User.first }

    it {
      subject.call
      expect(response.status).to eq(401)
    }

    context 'when request is authorized' do
      include_context 'with_authorized_user'

      it {
        subject.call
        expect(response.status).to eq(200)
      }

      it {
        subject.call
        expect(response.body).to eq([].to_json)
      }

      context "with account" do
        let!(:account) { create :account, user: User.first }
        let(:expected_result) {
          [{
             amount: 0,
             created_at: be,
             currency: "USD",
             id: be,
             updated_at: be,
             user_id: be
           }.stringify_keys]
        }
        it {
          subject.call
          expect(response.status).to eq(200)
        }

        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
      end
    end
  end

  describe 'GET show' do
    subject { -> { get :show, params: {id: account_id} } }

    context 'when request is authorized' do
      include_context 'with_authorized_user'

      context 'with existing account' do
        let(:account) { create :account, user: User.first }
        let(:account_id) { account.id }
        let(:expected_result) {
          {
            amount: 0,
            created_at: be,
            currency: "USD",
            id: be,
            updated_at: be,
            user_id: be
          }.stringify_keys
        }

        it {
          subject.call
          expect(response.status).to eq(200)
        }

        it {
          subject.call
          expect(JSON.parse(response.body)).to match(expected_result)
        }
      end

      context 'without existing account' do
        let(:account_id) { -1 }

        it {
          subject.call
          expect(response.status).to eq(404)
        }
      end
    end
  end

  describe 'POST create' do
    include_context 'with_authorized_user'

    subject { -> { post :create, params: {currency: 'USD'} } }

    let(:expected_result) {
      {
        amount: 0,
        created_at: be,
        currency: "USD",
        id: be,
        updated_at: be,
        user_id: be
      }.stringify_keys
    }

    it {
      subject.call
      expect(response.status).to eq(201)
    }

    it {
      subject.call
      expect(JSON.parse(response.body)).to match(expected_result)
    }

    it { expect { subject.call }.to change(Account, :count).by(1) }

    context 'when account currency raise conflict' do
      let!(:account) { create :account, currency: 'USD', user: User.first }

      it {
        subject.call
        expect(response.status).to eq(409)
      }
      it { expect { subject.call }.not_to change(Account, :count) }
    end
  end

  describe 'DELETE destroy' do
    include_context 'with_authorized_user'

    subject { -> { delete :destroy, params: {id: account_id} } }

    context 'with existing account' do
      let!(:account) { create :account, user: User.first }
      let(:account_id) { account.id }

      it {
        subject.call
        expect(response.status).to eq(204)
      }
      it { expect { subject.call }.to change(Account, :count).by(-1) }

      context 'with outcoming exchange_request' do
        let!(:account_to) { create :account, currency: 'CAD', user: User.first }
        let!(:exchange_request) { create :exchange_request, account_from: account, account_to: account_to }

        it {
          subject.call
          expect(response.status).to eq(204)
        }
        it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
        it { expect { subject.call }.to change(Account, :count).by(-1) }
      end

      context 'with incoming exchange_request' do
        let!(:account_from) { create :account, currency: 'CAD', user: User.first }
        let!(:exchange_request) { create :exchange_request, account_from: account_from, account_to: account }

        it {
          subject.call
          expect(response.status).to eq(204)
        }
        it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
        it { expect { subject.call }.to change(Account, :count).by(-1) }
      end
    end

    context 'without existing account' do
      let(:account_id) { -1 }

      it {
        subject.call
        expect(response.status).to eq(404)
      }
    end
  end

  describe 'POST top_up' do
    include_context 'with_authorized_user'

    subject { -> { post :top_up, params: {id: account_id, amount: amount}, as: :json } }

    let(:account) { create :account, user: User.first }
    let(:account_id) { account.id }
    let(:amount) { 1000 }
    let(:expected_result) {
      {
        amount: 1000,
        created_at: be,
        currency: "USD",
        id: be,
        updated_at: be,
        user_id: be
      }.stringify_keys
    }

    it {
      subject.call
      expect(response.status).to eq(200)
    }

    it {
      subject.call
      expect(JSON.parse(response.body)).to match(expected_result)
    }

    context 'with exchange_request' do
      let!(:exchange_request) { create :exchange_request, account_from: account }

      it {
        subject.call
        expect(response.status).to eq(200)
      }

      it {
        subject.call
        expect(JSON.parse(response.body)).to match(expected_result)
      }

      it {
        expect(PerformExchangeRequestJob).to receive(:perform_later).once
        subject.call
      }
    end
  end
end