RSpec.describe Authorization::CurrentUserService, type: :service do
  subject { -> { described_class.new(headers).call } }

  context 'with Authorization header' do
    let(:headers) { {'Authorization' => 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjdGTFB6ZlFZWHhyYkt4TVo0U0VJMyJ9.eyJpc3MiOiJodHRwczovL2Rldi1vNGdtNWg0cC5ldS5hdXRoMC5jb20vIiwic3ViIjoiZ29vZ2xlLW9hdXRoMnwxMDkwMjU1NjUyMjA0OTE3ODg0MzYiLCJhdWQiOlsiaHR0cHM6Ly9yYWlscy1zZWN1cmUtYXBpIiwiaHR0cHM6Ly9kZXYtbzRnbTVoNHAuZXUuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTYwNTM2NTE0MSwiZXhwIjoxNjA1NDUxNTQxLCJhenAiOiJaYTNTRERWbFlSNnhQSTd0enlkdU5QcnJUT2huRUpOTyIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwifQ.bIDdz3HCC9pclH9L-QUxPWRYZSlhpSB34iFfCBXZg5EqN58lWvSLawA0wj5nu_0opikyf713xyMCCB_sb_mGTQ6mNn2PR7pPEMXuprekkev2Imeg6VbXT4y6dG_eQh-N1PdmoveffEdsBj2IlH0serO6eQ0dHYuxnJiBcyWtHECU51gjptg5F-RxWAF3S11hT93TXLgEKpLIJOa9SvA2DnosuAB1iZxhjGV7gXZTobHDrP5cbSFBOXQxha37SWQAsCWHKd9f-_myRNUibbcHktSqMRW_AQNy9RCWaiMujia9-FC1L9NKf5KAigpOrpdday0-HhWL0t093K0WVfv_EQ'} }

    let(:expected_result) {
      [{"aud" => be,
        "azp" => "Za3SDDVlYR6xPI7tzyduNPrrTOhnEJNO",
        "exp" => 1605451541,
        "iat" => 1605365141,
        "iss" => "https://dev-o4gm5h4p.eu.auth0.com/",
        "scope" => "openid profile email",
        "sub" => "google-oauth2|109025565220491788436"},
       {"alg" => "RS256", "kid" => "7FLPzfQYXxrbKxMZ4SEI3", "typ" => "JWT"}]
    }

    it do
      Timecop.freeze(Time.new(2020, 11, 14, 19, 0, 0, "+04:00")) do
        user = subject.call
        expect(user.sub).to eq('google-oauth2|109025565220491788436')
      end
    end

    it do
      Timecop.freeze(Time.new(2020, 11, 14, 19, 0, 0, "+04:00")) do
        expect{subject.call}.to change(User, :count).by(1)
      end
    end

    context 'with existing user' do
      let!(:existing_user) { create :user, sub: 'google-oauth2|109025565220491788436'}

      it do
        Timecop.freeze(Time.new(2020, 11, 14, 19, 0, 0, "+04:00")) do
          expect(subject.call).to eq(existing_user)
        end
      end

      it do
        Timecop.freeze(Time.new(2020, 11, 14, 19, 0, 0, "+04:00")) do
          expect{subject.call}.not_to change(User, :count)
        end
      end
    end
  end
end