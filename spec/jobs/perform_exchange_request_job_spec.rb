# frozen_string_literal: true

RSpec.describe PerformExchangeRequestJob, type: :job do
  subject { -> { described_class.perform_now(request_id) } }

  context 'with exchange_request' do
    let(:request_id) { exchange_request.id }
    let!(:exchange_rate) { create :exchange_rate, from: 'USD', to: 'CAD', rate: 10 }
    let!(:account_to) { create :account, currency: 'CAD' }

    context "when exchange_request's datetime_before in past" do
      let!(:account_from) { create :account, currency: 'USD', amount: 10_000 }
      let!(:exchange_request) do
        create :exchange_request,
               account_from: account_from,
               account_to: account_to
      end

      around(:each) do |example|
        Timecop.freeze(exchange_request.datetime_before + 1.day) do
          example.run
        end
      end

      it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
      it { expect { subject.call }.not_to(change { account_from.reload.amount }) }
      it { expect { subject.call }.not_to(change { account_to.reload.amount }) }
    end

    context "when exchange_request's datetime_before in future" do
      context 'when account_from amount is insufficient' do
        let!(:account_from) { create :account, currency: 'USD', amount: 0 }
        let!(:exchange_request) do
          create :exchange_request,
                 account_from: account_from,
                 account_to: account_to
        end

        it { expect { subject.call }.not_to change(ExchangeRequest, :count) }
        it { expect { subject.call }.not_to(change { account_from.reload.amount }) }
        it { expect { subject.call }.not_to(change { account_to.reload.amount }) }
      end

      context 'when account_from amount is sufficient' do
        let!(:account_from) { create :account, currency: 'USD', amount: 10_000 }

        context 'when rate fits requested bounds' do
          context 'with both bounds defined' do
            let!(:exchange_request) do
              create :exchange_request,
                     account_from: account_from,
                     account_to: account_to,
                     min_exchange_rate: 8,
                     max_exchange_rate: 11
            end

            it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
            it { expect { subject.call }.to(change { account_from.reload.amount }.from(10_000).to(9000)) }
            it { expect { subject.call }.to(change { account_to.reload.amount }.from(0).to(10_000)) }
          end
          context 'with min bound defined' do
            let!(:exchange_request) do
              create :exchange_request,
                     account_from: account_from,
                     account_to: account_to,
                     min_exchange_rate: 8,
                     max_exchange_rate: nil
            end

            it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
            it { expect { subject.call }.to(change { account_from.reload.amount }.from(10_000).to(9000)) }
            it { expect { subject.call }.to(change { account_to.reload.amount }.from(0).to(10_000)) }
          end
          context 'with max bound defined' do
            let!(:exchange_request) do
              create :exchange_request,
                     account_from: account_from,
                     account_to: account_to,
                     min_exchange_rate: nil,
                     max_exchange_rate: 11
            end

            it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
            it { expect { subject.call }.to(change { account_from.reload.amount }.from(10_000).to(9000)) }
            it { expect { subject.call }.to(change { account_to.reload.amount }.from(0).to(10_000)) }
          end
        end

        context "when rate doesn't fits bounds" do
          context 'with both bounds defined' do
            let!(:exchange_request) do
              create :exchange_request,
                     account_from: account_from,
                     account_to: account_to,
                     min_exchange_rate: 80,
                     max_exchange_rate: 110
            end

            it { expect { subject.call }.not_to change(ExchangeRequest, :count) }
            it { expect { subject.call }.not_to(change { account_from.reload.amount }) }
            it { expect { subject.call }.not_to(change { account_to.reload.amount }) }
          end

          context 'with min bound defined' do
            let!(:exchange_request) do
              create :exchange_request,
                     account_from: account_from,
                     account_to: account_to,
                     min_exchange_rate: 80,
                     max_exchange_rate: nil
            end

            it { expect { subject.call }.not_to change(ExchangeRequest, :count) }
            it { expect { subject.call }.not_to(change { account_from.reload.amount }) }
            it { expect { subject.call }.not_to(change { account_to.reload.amount }) }
          end

          context 'with max bound defined' do
            let!(:exchange_request) do
              create :exchange_request,
                     account_from: account_from,
                     account_to: account_to,
                     min_exchange_rate: nil,
                     max_exchange_rate: 1
            end

            it { expect { subject.call }.not_to change(ExchangeRequest, :count) }
            it { expect { subject.call }.not_to(change { account_from.reload.amount }) }
            it { expect { subject.call }.not_to(change { account_to.reload.amount }) }
          end
        end

        context 'when bounds not defined' do
          let!(:exchange_request) do
            create :exchange_request,
                   account_from: account_from,
                   account_to: account_to,
                   min_exchange_rate: nil,
                   max_exchange_rate: nil
          end

          it { expect { subject.call }.to change(ExchangeRequest, :count).by(-1) }
          it { expect { subject.call }.to(change { account_from.reload.amount }.from(10_000).to(9000)) }
          it { expect { subject.call }.to(change { account_to.reload.amount }.from(0).to(10_000)) }
        end
      end
    end
  end

  context 'without exchange_request' do
    let(:request_id) { 0 }

    it { expect { subject.call }.not_to change(ExchangeRequest, :count) }
    it { expect { subject.call }.not_to raise_error(ActiveRecord::RecordNotFound) }
  end
end
