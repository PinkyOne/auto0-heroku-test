
RSpec.shared_context 'with_authorized_user' do
  before do
    user = create :user, sub: 'test_sub'
    allow(JsonWebToken).to receive(:verify).and_return(true)
    allow(JsonWebToken).to receive(:extract_sub).and_return(user.sub)
  end
end
