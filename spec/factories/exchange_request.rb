# frozen_string_literal: true

FactoryBot.define do
  factory :exchange_request do
    amount { 1000 }
    datetime_before { Time.now + 1.minute }
    min_exchange_rate { 100 }
    max_exchange_rate { 1000 }
    association :account_from, factory: :account
    association :account_to, factory: :account
  end
end
