# frozen_string_literal: true

FactoryBot.define do
  factory :exchange_rate do
    from { 'USD' }
    to { 'CAD' }
    rate { 10 }
  end
end
