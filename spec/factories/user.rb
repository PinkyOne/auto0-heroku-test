# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sub { 'test_sub' }
  end
end
