# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    currency { 'USD' }
    amount { 0 }
    association :user
  end
end
