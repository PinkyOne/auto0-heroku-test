# frozen_string_literal: true

class CreateExchangeRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :exchange_requests do |t|
      t.references :account_from, null: false, foreign_key: { to_table: :accounts }
      t.references :account_to, null: false, foreign_key: { to_table: :accounts }
      t.integer :amount
      t.decimal :exchange_rate
      t.datetime :datetime_before

      t.timestamps
    end
  end
end
