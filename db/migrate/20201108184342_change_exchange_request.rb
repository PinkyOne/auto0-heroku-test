class ChangeExchangeRequest < ActiveRecord::Migration[6.0]
  def up
    remove_column :exchange_requests, :exchange_rate
    add_column :exchange_requests, :min_exchange_rate, :decimal
    add_column :exchange_requests, :max_exchange_rate, :decimal
  end

  def down
    add_column :exchange_requests, :exchange_rate, :decimal
    remove_column :exchange_requests, :min_exchange_rate
    remove_column :exchange_requests, :max_exchange_rate
  end
end
