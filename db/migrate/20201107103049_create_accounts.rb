# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[6.0]
  def up
    enable_extension "citext"

    create_table :accounts do |t|
      t.citext :currency, null: false
      t.integer :amount, default: 0
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :accounts, %i[user_id currency], unique: true
  end

  def down
    remove_index :accounts, name: 'index_accounts_on_user_id_and_currency'
    drop_table :accounts
    disable_extension "citext"
  end
end
