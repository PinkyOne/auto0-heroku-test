class ChangeExchangeRequestDatetimeBefore < ActiveRecord::Migration[6.0]
  def change
    change_column_null :exchange_requests, :datetime_before, false
  end
end
