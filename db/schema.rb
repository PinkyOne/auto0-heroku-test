# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_14_194335) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "citext"
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.citext "currency", null: false
    t.integer "amount", default: 0
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id", "currency"], name: "index_accounts_on_user_id_and_currency", unique: true
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "exchange_rates", force: :cascade do |t|
    t.string "from"
    t.string "to"
    t.float "rate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "exchange_requests", force: :cascade do |t|
    t.bigint "account_from_id", null: false
    t.bigint "account_to_id", null: false
    t.integer "amount"
    t.datetime "datetime_before", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "min_exchange_rate"
    t.decimal "max_exchange_rate"
    t.index ["account_from_id"], name: "index_exchange_requests_on_account_from_id"
    t.index ["account_to_id"], name: "index_exchange_requests_on_account_to_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "sub"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "accounts", "users"
  add_foreign_key "exchange_requests", "accounts", column: "account_from_id"
  add_foreign_key "exchange_requests", "accounts", column: "account_to_id"
end
