# README

# Ruby version
ruby-2.6.3
# Rails version
6.0.3.4

# Configuration
* Ask project owner for the master key
* You may set up custom `REDIS_URL` 
* You need to set up `DB_HOST, DB_USER, DB_PASSWORD` environment variables to run app locally
## Docker
You also have the option to run this application in Docker containers. There is instructions for the first run:
```
docker-compose up -d --build rails sidekiq
docker-compose run rails rake db:create db:migrate
```
At this moment everything is ready
### Troubleshooting
If you have problems with running rails or sidekiq service try launch it via separate commands
```
docker-compose up -d --build rails
docker-compose up -d --build sidekiq
```
### Exchange rates initialization or refreshing
Be aware of using it because it calls external API for fetching rates. On the Free plan it provides only 1000 requests per month.
```
docker-compose run rails rake open_exchange_rates:refresh_rates
```
### Rspec
For the tests run use the next command:
```
docker-compose run -e "RAILS_ENV=test" rails bundle exec rspec
```
# Database creation
```
bundle exec rails db:create
```
# Database initialization
```
bundle exec rails db:migrate
```
# How to run the test suite
```
bundle exec rspec
```
With Coverage:
```
COVERAGE=true bundle exec rspec
```
# Services (job queues, cache servers, search engines, etc.)
* `sidekiq` for running jobs
* `redis` as a cache service
# Deployment instructions

## This app is hosted on heroku
Run next command to deploy:
```
git push heroku master
```
Requires `heroku remote` set up

# Authorization service
### Description
This app is using [auth0](https://auth0.com/docs/quickstart/backend/rails/01-authorization) as Authorization service

It provides JWT tokens which allows us to authorize incoming requests.

It also allows us to extract the user's sub and we can use that to identify the current user.
### TODO
* Use Redis for caching jwks
* Use Redis for caching user's verified JWT tokens

# Exchange rates service
We are using [Open exchange rates](https://openexchangerates.org/) service for getting and refreshing currency exchange rates
## Configuring
Paste your oxr `app_id` into encrypted rails credentials under oxr.app_id section

To edit credentials use next command:
```
EDITOR='vim' rails credentials:edit --environment development
```
Ask project owner for the development.key if you need some
## Refreshing rates
Run next command
```
rake open_exchange_rates:refresh_rates
```

In production, this rake task has been configured in the scheduler service to run every hour.

# TODO
* Set up sentry or something else for logging errors in production