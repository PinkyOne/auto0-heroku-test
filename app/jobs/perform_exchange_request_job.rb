# frozen_string_literal: true

class PerformExchangeRequestJob < ApplicationJob
  queue_as :default

  def perform(request_id)
    request = ExchangeRequest.find(request_id)
    request.with_lock do
      # reload call will raise RecordNotFound to avoid race condition
      # More complex solution is to use sidekiq uniq job
      # (Sidekiq Ent edition/free gems/custom middleware implementation)
      request.reload
      if check_datetime_before(request)
        request.delete
        return
      end
      return if check_account_from_amount(request)

      return unless rate_fits_bounds(request)

      perform_request(request)
    end
  rescue ActiveRecord::RecordNotFound
    nil
  end

  private

  def check_datetime_before(request)
    request.datetime_before < Time.zone.now
  end

  def check_account_from_amount(request)
    request.account_from.amount < request.amount
  end

  def rate_fits_bounds(request)
    current_rate = Money.default_bank.get_rate(request.account_from.currency, request.account_to.currency)

    min_rate = request.min_exchange_rate || -Float::INFINITY
    max_rate = request.max_exchange_rate || Float::INFINITY
    min_rate <= current_rate && current_rate <= max_rate
  end

  def perform_request(request)
    ActiveRecord::Base.transaction do
      account_from = request.account_from
      account_to = request.account_to

      exchanged_amount = Money.new(request.amount, account_from.currency).exchange_to(account_to.currency)
      account_from.update(amount: account_from.amount - request.amount)
      account_to.update(amount: account_to.amount + exchanged_amount.cents)
      request.delete
    end
  end
end
