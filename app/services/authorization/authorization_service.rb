# frozen_string_literal: true
module Authorization
  class AuthorizationService
    include Common

    def initialize(headers = {})
      @headers = headers
    end

    def call
      JsonWebToken.verify(http_token)
    end
  end
end
