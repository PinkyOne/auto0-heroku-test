module Authorization
  class CurrentUserService
    include Common

    def initialize(headers = {})
      @headers = headers
    end

    def call
      User.find_or_create_by!(sub: user_sub)
    end

    private

    def user_sub
      JsonWebToken.extract_sub(http_token)
    end
  end
end
