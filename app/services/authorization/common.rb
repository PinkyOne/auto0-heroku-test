module Authorization
  module Common
    private

    def http_token
      @headers['Authorization'].split(' ').last if @headers['Authorization'].present?
    end
  end
end
