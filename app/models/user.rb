class User < ApplicationRecord
  has_many :accounts
  has_many :income_exchange_requests, through: :accounts
end
