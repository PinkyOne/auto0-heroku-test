class Account < ApplicationRecord
  belongs_to :user
  has_many :income_exchange_requests, class_name: 'ExchangeRequest', foreign_key: :account_to_id, dependent: :destroy
  has_many :outcome_exchange_requests, class_name: 'ExchangeRequest', foreign_key: :account_from_id, dependent: :destroy
end
