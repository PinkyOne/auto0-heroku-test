class AccountsController < SecuredController
  def index
    accounts = current_user.accounts.all
    render json: accounts
  end

  def show
    account = current_user.accounts.find(params[:id])
    render json: account
  end

  def create
    account = current_user.accounts.create!(account_params)
    render json: account, status: :created
  rescue ActiveRecord::RecordNotUnique
    head :conflict
  end

  def destroy
    account = current_user.accounts.find(params[:id])
    account.destroy
    head :no_content
  end

  def top_up
    account = current_user.accounts.find(params[:id])
    account.amount += params[:amount]
    account.save!

    account.outcome_exchange_requests.each do |request|
      PerformExchangeRequestJob.perform_later(request.id)
    end

    render json: account
  end

  private

  def account_params
    params.permit(:currency)
  end
end
