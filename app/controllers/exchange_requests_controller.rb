# frozen_string_literal: true

class ExchangeRequestsController < SecuredController
  include Create

  def index
    income_exchange_requests = current_user.income_exchange_requests
    render json: income_exchange_requests, include: {account_from: {only: :currency}, account_to: {only: :currency}}
  end

  def destroy
    exchange_request = current_user.income_exchange_requests.find(params[:id])
    exchange_request.delete
    head :no_content
  end

  private

  def bad_request_response(message)
    render(json: {message: message},
           status: :bad_request)
  end

  def exchange_request_params
    params.permit(:currency_from, :currency_to, :amount, :min_exchange_rate, :max_exchange_rate, :datetime_before)
  end
end
