class SecuredController < ApplicationController
  before_action :authorize_request

  private

  def authorize_request
    Authorization::AuthorizationService.new(request.headers).call
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  def current_user
    @current_user ||= Authorization::CurrentUserService.new(request.headers).call
  end

end
