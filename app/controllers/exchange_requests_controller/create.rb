class ExchangeRequestsController
  module Create
    def create
      account_from = current_user.accounts.find_by(currency: exchange_request_params[:currency_from])
      min_rate = exchange_request_params[:min_exchange_rate]
      max_rate = exchange_request_params[:max_exchange_rate]

      validation_error = validate_request(account_from, min_rate, max_rate)
      return validation_error if validation_error

      exchange_request = create_request(account_from, max_rate, min_rate)

      PerformExchangeRequestJob.perform_later(exchange_request.id)

      render json: exchange_request, status: :created
    end

    private

    def validate_request(account_from, min_rate, max_rate)
      return bad_request_response(I18n.t('api.exchange_requests.errors.currency_error')) unless check_currencies
      return bad_request_response(I18n.t('api.exchange_requests.errors.account_from_not_found', currency_from: exchange_request_params[:currency_from])) unless account_from
      return bad_request_response(I18n.t('api.exchange_requests.errors.datetime_before_missing')) unless exchange_request_params[:datetime_before]
      return bad_request_response(I18n.t('api.exchange_requests.errors.exchange_rules_missing')) unless check_bounds_presence(max_rate, min_rate)
      return bad_request_response(I18n.t('api.exchange_requests.errors.min_higher_than_max')) if check_bounds(max_rate, min_rate)

      nil
    end

    def check_currencies
      currency_from = exchange_request_params[:currency_from]
      currency_to = exchange_request_params[:currency_to]
      return false unless currency_from && currency_to

      Money.default_bank.get_rate(currency_from, currency_to)
    rescue Money::Currency::UnknownCurrency
      false
    end

    def check_bounds_presence(max_rate, min_rate)
      min_rate || max_rate
    end

    def check_bounds(max_rate, min_rate)
      min_rate && max_rate && min_rate > max_rate
    end

    def create_request(account_from, max_rate, min_rate)
      account_to = current_user.accounts.find_or_create_by!(currency: exchange_request_params[:currency_to])
      ExchangeRequest.create!(
        account_from: account_from,
        account_to: account_to,
        amount: exchange_request_params[:amount],
        min_exchange_rate: min_rate,
        max_exchange_rate: max_rate,
        datetime_before: exchange_request_params[:datetime_before]
      )
    end
  end
end