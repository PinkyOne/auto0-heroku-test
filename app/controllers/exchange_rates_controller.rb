class ExchangeRatesController < ApplicationController
  def index
    rates = ExchangeRate.all
    render json: rates
  end
end
