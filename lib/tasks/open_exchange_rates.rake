# frozen_string_literal: true

# Add every hour crontab for this task
namespace :open_exchange_rates do
  desc 'Refresh rates from cache and update rates'
  task refresh_rates: :environment do
    Money.default_bank.refresh_rates
    Money.default_bank.update_rates

    ExchangeRequest.all.pluck(:id).each do |request_id|
      PerformExchangeRequestJob.perform_later(request_id)
    end
  end
end
