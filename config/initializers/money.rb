# frozen_string_literal: true

require 'money'
require 'money/bank/open_exchange_rates_bank'

oxr = Money::Bank::OpenExchangeRatesBank.new('ExchangeRate')
oxr.app_id = Rails.application.credentials.oxr[:app_id]

oxr.cache = if Rails.env.test?
              Rails.root.join('spec/fixtures/currency-rates.json').to_s
            else
              proc do |value|
                key = 'exchange_rates'
                if value
                  REDIS.set(key, value)
                else
                  REDIS.get(key)
                end
              end
            end

Money.default_bank = oxr

# puts Money.default_bank.get_rate('USD', 'CAD')
