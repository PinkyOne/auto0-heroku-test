# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  resources :accounts
  post '/accounts/:id/top_up', to: 'accounts#top_up'
  resources :exchange_rates
  resources :exchange_requests

  mount Sidekiq::Web => '/sidekiq'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
